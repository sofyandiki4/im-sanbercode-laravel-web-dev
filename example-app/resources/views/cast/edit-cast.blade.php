@extends('layouts.master')

@section('title')
Edit Cast
@endsection

@section('sub-title')
Form Edit Cast
@endsection

@section('content')

<form action="/cast/{{ $casts->id }}" method="POST">
    @csrf
    @method('put')
    <div class="form-row mb-2">
        <div class="col-9">
            <label for="exampleFormControlTextarea1">Nama</label>
            <input type="text" class="form-control" value="{{ $casts->nama }}" name="nama">
        </div>
        <div class="col">
            <label for="exampleFormControlTextarea1">Umur</label>
            <input type="text" class="form-control" value="{{ $casts->umur }}" name="umur">
        </div>
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group mt-3">
        <label for="exampleFormControlTextarea1">Biodata</label>
        <textarea class="form-control"  name="bio" cols="5" rows="10">{{ $casts->bio }}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
