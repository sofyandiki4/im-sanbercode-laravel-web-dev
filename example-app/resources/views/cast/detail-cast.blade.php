@extends('layouts.master')

@section('title')
Detail Cast
@endsection

@section('sub-title')
Form Detail Cast
@endsection

@section('content')

<h1>{{ $casts->nama }}</h1>
<h3>Umur : {{ $casts->umur }}</h3>
<p>{{ $casts->bio }}</p>

<a href="/cast" class="btn btn-secondary mb-2">Kembali</a>

@endsection
