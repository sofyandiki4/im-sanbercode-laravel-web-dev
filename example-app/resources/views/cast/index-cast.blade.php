@extends('layouts.master')

@section('title')
Tampil Cast
@endsection

@section('sub-title')
Form Tampil Cast
@endsection

@section('content')

<a href="/cast/create" class="btn btn-secondary mb-2">Tambah Cast</a>

<table class="table table-striped table-dark">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($casts as $key => $item)
    <tr>
        <th scope="row">{{ $key + 1 }}</th>
        <td>{{ $item->nama }}</td>
        <td>
            <form action="/cast/{{ $item->id }}" method="POST" >
                @csrf
                @method('delete')
            <a href="/cast/{{ $item->id }}" class="btn btn-info btn-sm mb-2">Detail</a>
            <a href="/cast/{{ $item->id }}/edit" class="btn btn-warning btn-sm mb-2">edit</a>
            <input type="submit" value="delete" class="btn btn-danger btn-sm mb-2">
            </form>
        </td>
      </tr>
    @empty
    <tr>
        <td>Data Cast</td>
      </tr>
    @endforelse
  </tbody>
</table>

@endsection
