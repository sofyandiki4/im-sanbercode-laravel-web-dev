@extends('layouts.master')

@section('title')
Tambah Cast
@endsection

@section('sub-title')
Form Tambah Cast
@endsection

@section('content')

<form action="/cast" method="POST">
    @csrf
    <div class="form-row mb-2">
        <div class="col-9">
            <label for="exampleFormControlTextarea1">Nama</label>
            <input type="text" class="form-control" placeholder="Nama" name="nama">
        </div>
        <div class="col">
            <label for="exampleFormControlTextarea1">Umur</label>
            <input type="text" class="form-control" placeholder="Umur" name="umur">
        </div>
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group mt-3">
        <label for="exampleFormControlTextarea1">Biodata</label>
        <textarea class="form-control" placeholder="Biodata" name="bio" cols="5" rows="10"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
