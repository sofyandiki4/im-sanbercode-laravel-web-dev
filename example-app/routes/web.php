<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\HomeController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'indexHome']);
Route::get('/register', [AuthController::class, 'indexRegister']);
Route::get('/welcome', [AuthController::class, 'indexWelcome']);

Route::post('/register/create', [AuthController::class, 'createRegister']);

// Route::get('/master', [HomeController::class, 'indexMaster']);
Route::get('/table', [HomeController::class, 'indexTable']);
Route::get('/data-table', [HomeController::class, 'indexDatatable']);

//CRUD

//tampil tambah data
Route::get('/cast/create', [CastController::class, 'createCast']);

//tambah data
Route::post('/cast', [CastController::class, 'storeCast']);

//tampil data
Route::get('/cast', [CastController::class, 'indexCast']);


//tampil data per id
Route::get('/cast/{id}', [CastController::class, 'showCast']);

//tampil edit data per id
Route::get('/cast/{id}/edit', [CastController::class, 'editCast']);

//edit data per id
Route::put('/cast/{id}', [CastController::class, 'updateCast']);

//delete data per id
Route::delete('/cast/{id}', [CastController::class, 'destroyCast']);