<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function indexRegister () {
        return view('register');
    }

    public function createRegister (Request $request) {
    //    dd($request);
    $namaDepan = $request['fname'];
    $namaBelakang = $request['lname'];

    return view('welcome',['namaDepan' => $namaDepan, 'namaBelakang' => $namaBelakang]);

    }

    public function indexWelcome () {
        return view('welcome');
    }
}
