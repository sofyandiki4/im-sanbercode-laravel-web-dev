<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function indexHome () {
        return view('home');
    }

    public function indexTable () {
        return view('table');
    }

    public function indexDatatable () {
        return view('data-table');
    }

    // public function indexMaster () {
    //     return view('layouts.master');
    // }
}
