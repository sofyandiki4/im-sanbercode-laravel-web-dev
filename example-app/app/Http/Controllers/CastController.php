<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function createCast() {
        return view('cast.tambah-cast');
    }

    public function storeCast(Request $request) {
        $request->validate([
            'nama' => 'required| max:45',
            'umur' => 'required| integer',
            'bio'  => 'required',
        ],
        [
            'nama.required' => "nama tidak boleh kosong",
            'nama.max:45' => "nama tidak boleh lebih dari 45 huruf",
            'umur.required' => "umur tidak boleh kosong",
            'umur.integer' => "umur harus angka",
            'bio.required' => "bio tidak boleh kosong",
        ]);

        DB::table('casts')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]
    );

        return redirect('/cast');

    }

    public function indexCast() {

        $casts = DB::table('casts')->get();

        return view('cast.index-cast',['casts' => $casts]);
    }

    public function showCast($id) {
        
        $casts = DB::table('casts')->find($id);

        return view('cast.detail-cast',['casts' => $casts]);
    }

    public function editCast($id) {
        
        $casts = DB::table('casts')->find($id);

        return view('cast.edit-cast',['casts' => $casts]);
    }

    public function updateCast($id,Request $request) {
        
        $request->validate([
            'nama' => 'required| max:45',
            'umur' => 'required| integer',
            'bio'  => 'required',
        ],
        [
            'nama.required' => "nama tidak boleh kosong",
            'nama.max:45' => "nama tidak boleh lebih dari 45 huruf",
            'umur.required' => "umur tidak boleh kosong",
            'umur.integer' => "umur harus angka",
            'bio.required' => "bio tidak boleh kosong",
        ]);

        DB::table('casts')->where('id', $id)->update([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
        ]
    );

        return redirect('/cast');
    }

    public function destroyCast($id) {
        
        $casts = DB::table('casts')->where('id','=',$id)->delete();

        return redirect('/cast');
    }

}
